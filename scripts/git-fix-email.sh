#!/bin/bash

DEFAULT_NAME=Ben
DEFAULT_EMAIL="3760277-2.b.curious@users.noreply.gitlab.com"

WRONG_EMAIL=$1
CORRECTED_NAME="${2:-$DEFAULT_NAME}"
CORRECTED_EMAIL="${3:-$DEFAULT_EMAIL}"

echo "*** WARNING ***"
echo "This will REWRITE git history!"
echo "Replacing email address \"${WRONG_EMAIL}\" with \"${CORRECTED_NAME}\" <${CORRECTED_EMAIL}>"

read -n 1 -s -r -p "Press control-c to abort or any other key to continue."

git filter-branch --env-filter "
if [ \"\$GIT_COMMITTER_EMAIL\" = \"$WRONG_EMAIL\" ]
then
    export GIT_COMMITTER_NAME=\"$CORRECTED_NAME\"
    export GIT_COMMITTER_EMAIL=\"$CORRECTED_EMAIL\"
fi
if [ \"\$GIT_AUTHOR_EMAIL\" = \"$WRONG_EMAIL\" ]
then
    export GIT_AUTHOR_NAME=\"$CORRECTED_NAME\"
    export GIT_AUTHOR_EMAIL=\"$CORRECTED_EMAIL\"
fi
" --tag-name-filter cat -- --branches --tags
