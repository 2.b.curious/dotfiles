
solarized_colorset() {
	typeset -g sol_base_d3=#002b36
	typeset -g sol_base_d2=#073642
	typeset -g sol_base_d1=#586e75
	typeset -g sol_base_d0=#657b83
	typeset -g sol_base_l0=#839496
	typeset -g sol_base_l1=#93a1a1
	typeset -g sol_base_l2=#eee8d5
	typeset -g sol_base_l3=#fdf6e3
	typeset -g sol_yellow=#b58900
	typeset -g sol_orange=#cb4b16
	typeset -g sol_red=#dc322f
	typeset -g sol_magenta=#d33682
	typeset -g sol_violet=#6c71c4
	typeset -g sol_blue=#268bd2
	typeset -g sol_cyan=#2aa198
	typeset -g sol_green=#859900
}

solarized_grey_colorset() {
	typeset -g sol_base_d3=#272727
	typeset -g sol_base_d2=#313131
	typeset -g sol_base_d1=#6A6A6A
	typeset -g sol_base_d0=#777777
	typeset -g sol_base_l0=#919191
	typeset -g sol_base_l1=#9E9E9E
	typeset -g sol_base_l2=#E8E8E8
	typeset -g sol_base_l3=#F6F6F6
	typeset -g sol_yellow=#b58900
	typeset -g sol_orange=#cb4b16
	typeset -g sol_red=#dc322f
	typeset -g sol_magenta=#d33682
	typeset -g sol_violet=#6c71c4
	typeset -g sol_blue=#268bd2
	typeset -g sol_cyan=#2aa198
	typeset -g sol_green=#859900
}


solarized_grey_colorset
typeset -g sol_bkg_def=$sol_base_d3
typeset -g sol_bkg_hi=$sol_base_d2
typeset -g sol_secondary=$sol_base_d1
typeset -g sol_primary=$sol_base_l0
typeset -g sol_emphasis=$sol_base_l1
